# Lesson 3

## Objectives

1. Learn the principles of the formation of nouns of the first declension.
1. Learning the declension of βουλή, καλὴ βουλή, and write out the declension of κλαγγή and Χρύση.
1. Learn the rules of syntax: 970, 1011, 1025.
