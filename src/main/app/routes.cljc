(ns app.routes
  #?(:cljs (:require-macros [cljs.core.async.macros :refer [go]]))
  (:require
   [schema.core :as s]
   #?(:clj [clojure.data.json :as json])
   #?(:clj [ring.util.response :refer [get-header]])
   #?(:clj [rum.core :as rum])
   #?(:clj [app.api :as api])
   #?(:clj [app.config :refer [config]])
   #?(:clj [app.transit :as transit])
   #?(:clj [app.ui :as ui])
   #?(:cljs [cljs.core.async :refer [<!]])
   #?(:cljs [cljs-http.client :as http])
   #?(:cljs [cognitect.transit :as transit])
   #?(:cljs [app.state])))

#?(:clj
   (do
     (defn html-response [component state]
       {:status  200
        :headers {"Content-Type" "text/html; charset=utf-8"}
        :body    (str "<!DOCTYPE html>\n"
                      (rum/render-html
                       (ui/Page {:scripts [(get-in (config) [:assets :cljs])]
                                 :state state} component)))})

     (defn json-response [payload]
       {:status 200
        :headers {"Content-Type" "application/json; charset=utf-8"}
        :body (json/write-str payload)})

     (defn transit-response [payload]
       {:status  200
        :headers {"Content-Type" "application/transit+json; charset=utf-8"}
        :body    (transit/write-transit-str payload)})

     (defn match-data [req]
       (let [route-name (get-in req [:reitit.core/match :data :name])
             parameters (:parameters req)]
         {:name route-name
          :parameters parameters}))

     (defn lesson-data [req]
       (when-let [lesson-id (get-in req [:parameters :path :lesson-id])]
         (api/lesson lesson-id)))

     (defn get-state [req]
       {:lesson (lesson-data req)
        :match (match-data req)})

     (defn home-handler [req]
       (html-response ui/Root (get-state req)))

     (defn create-lesson-handler [req] {:status 201 :body "created lesson"})
     (defn lessons-handler [_req] {:status 200 :body "lessons"})

     (defn lesson-handler [req]
       (let [state (get-state req)]
         (case (get-header req "accept")
           "application/json" (json-response {:data state})
           "application/transit+json" (transit-response {:data state})
           (html-response ui/Root state))))

     (defn lesson-edit-handler [_req] {:status 200 :body "lesson-edit"})
     (defn new-lesson-handler [req]
       (html-response ui/Root (get-state req)))

     (defn login-handler [_req] {:status 200 :body "Login"})
     (defn sign-up-handler [_req] {:status 200 :body "Sign-up"})
     (defn users-handler [_req] {:status 200 :body "users"})
     (defn user-handler [_req] {:status 200 :body "user"})
     (defn user-edit-handler [_req] {:status 200 :body "user-edit"})
     (defn user-login-handler [_req] {:status 200 :body "Log in user"})
     (defn user-new-handler [_req] {:status 200 :body "New user"})))

#?(:cljs
   (defn get-lesson-data [id]
     (go (let [response (<! (http/get
                             (str "/lessons/" id)
                             {:accept "application/transit+json"}))
               lesson (-> response :body :data :lesson)]
           (if lesson
             (swap! app.state/state assoc :lesson lesson)
             (println response))))))

(def routes
  [""
   ["/" {:name ::home
         #?@(:clj [:get {:handler home-handler}])}]
   ["/login" {:name ::login
              #?@(:clj [:get {:handler login-handler}])}]
   ["/signup" {:name ::sign-up
               #?@(:clj [:get {:handler sign-up-handler}])}]
   ["/lessons"
    ["" {:name ::lessons
         #?@(:clj [:get {:handler lessons-handler}])}]
    ["/:lesson-id"
     ["" {:name ::lesson
          :parameters {:path {:lesson-id s/Int}
                       :query {(s/optional-key :study) s/Str}}
          #?@(:clj [:get {:handler lesson-handler}])
          #?@(:cljs [:controllers
                     [{:parameters {:path [:lesson-id]}
                       :start (fn [{:keys [path]}]
                                (let [lesson-id (:lesson-id path)]
                                  (js/console.log "Entering lesson" lesson-id)
                                  (when-not (= lesson-id (get-in @app.state/state [:lesson :id]))
                                    (get-lesson-data lesson-id))))
                       :stop (fn [{:keys [path]}]
                               (js/console.log
                                "Leaving lesson"
                                (:lesson-id path)))}]])}]]]
   ["/new"
    ["/lesson" {:name ::new-lesson
                #?@(:clj [:get new-lesson-handler])}]]])
