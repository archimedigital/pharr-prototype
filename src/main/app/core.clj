(ns app.core
  (:gen-class)
  (:require [org.httpkit.server :as http-kit]
            [integrant.core :as ig]
            [muuntaja.core :as m]
            [reitit.coercion]
            [reitit.coercion.schema :as rcs]
            [reitit.dev.pretty :as pretty]
            [reitit.http :as http]
            [reitit.http.coercion :as coercion]
            [reitit.http.interceptors.exception :as exception]
            [reitit.http.interceptors.multipart :as multipart]
            [reitit.http.interceptors.muuntaja :as muuntaja]
            [reitit.http.interceptors.parameters :as parameters]
            [reitit.interceptor.sieppari :as sieppari]
            [ring.middleware.cookies :refer [wrap-cookies]]
            [ring.middleware.head :refer [wrap-head]]
            [ring.middleware.reload :refer [wrap-reload]]
            [reitit.ring :as ring]
            [reitit.ring.spec :as rrs]
            [app.config :refer [config]]
            [app.routes :refer [routes]]))

(defonce system (atom nil))

(def router
  (http/router
   (into routes [["/assets/*" (ring/create-resource-handler)]])
   {:data {:coercion rcs/coercion
           :interceptors [;; query-params & form-params
                          (parameters/parameters-interceptor)
                          ;; content-negotiation
                          (muuntaja/format-negotiate-interceptor)
                          ;; encoding response body
                          (muuntaja/format-response-interceptor)
                          ;; exception handling
                          (exception/exception-interceptor)
                          ;; decoding request body
                          (muuntaja/format-request-interceptor)
                          ;; coercing response bodies
                          (coercion/coerce-response-interceptor)
                          ;; coercing request parameters
                          (coercion/coerce-request-interceptor)
                          ;; multipart
                          (multipart/multipart-interceptor)]
           :muuntaja m/instance}
    :exception pretty/exception
    :validate rrs/validate}))

(def handler
  (http/ring-handler
   router
   (ring/routes
    (ring/create-default-handler))
   {:executor sieppari/executor}))

(defmethod ig/init-key :server [_ {:keys [port]}]
  (println "Server running on port" port)
  (-> (if (:dev (config))
        (wrap-reload #'handler)
        handler)
      (wrap-cookies)
      (wrap-head)
      (http-kit/run-server {:port (Integer/parseInt port)})))

(defmethod ig/halt-key! :server [_ server]
  (server :timeout 100)
  (println "Server stopped")
  (reset! system nil))

(defmethod ig/init-key :db [_ {:keys [uri]}]
  (println "Connecting to database" uri))

(defmethod ig/halt-key! :db [_ {:keys [uri]}]
  (println "Disconnecting from database" uri))

(defmethod ig/init-key :dev [_ opts]
  (println "Development mode initiated?" opts))

(defmethod ig/halt-key! :dev [_ _])

(defn -main []
  (reset! system (ig/init (config [:db :dev :server]))))
