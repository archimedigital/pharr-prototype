(ns app.config
  (:require [aero.core :refer [read-config]]
            [clojure.java.io :as io]))

(defn config
  ([] (read-config (io/resource "config.edn")
                   {:profile
                    (keyword (System/getenv "ENV"))}))
  ([ks] (select-keys (config) ks)))
