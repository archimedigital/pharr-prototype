(ns user
  (:require [clojure.tools.namespace.repl :refer [refresh]]
            [integrant.repl :as ig-repl]
            [app.config :refer [config]]
            [app.core :as core]))

(ig-repl/set-prep!
 (constantly (config [:dev :db :server])))

(defn go []
  (ig-repl/go))

(defn reset []
  (ig-repl/halt)
  (refresh :after 'user/go))
