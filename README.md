Pharr's Homer (by The New Alexandria Foundation)
------

# Development

## Developing with Calva

After cloning the repo, you should be able to get up and running fairly quickly
using VS Code/Codium and [Calva](https://calva.io/). To get started, open the
Command Palette (`cmd+shift+P`) and type `calva`, then select "Start a Project
REPL and Connect (aka Jack-In)".

The "project type" is "deps.edn + Fighweel Main," and you'll want to select the
`:dev` and `:fig` aliases (defined in the `deps.edn` file).

> Note: You might see a warning about other aliases, but you can safely ignore
> them. They're used by CIDER when developing in Emacs.

Once you're in the REPL, the Figwheel server should build the front-end code
automatically. To get the backend server running, simply execute `(go)` (defined
in `src/dev/user.clj`) at the REPL, then open your browser to `localhost:8080`.

## Developing with Emacs

Emacs setup is even easier, thanks to the `.dir-locals.el` config. Simply run
`M-x cider-jack-in-clj&cljs` (`C-c C-x C-j RET`), and you'll get two REPLs, one
for Figwheel Main and one for the backend. Execute `(go)` in the backend REPL
  and open your browser, then start developing.

# Deployment

TODO

# Linting

This project uses [clj-kondo](https://github.com/clj-kondo/clj-kondo) for
linting. You can follow the installation and setup instructions found
clj-kondo's the project's README. The `.clj-kondo/.cache/` folder is
git-ignored, so you won't be bothered by it if you don't want linting. (But
really, why wouldn't you want linting?)

You can feel free to add to the `.clj-kondo/config.edn` file as needed. See
[clj-kondo
configuration](https://github.com/clj-kondo/clj-kondo/blob/master/doc/config.md)
for more info.

# Credits

Much of the original project boilerplate was borrowed from
[@tonsky's](https://github.com/tonsky) https://github.com/tonsky/grumpy.

# License

The MIT License (MIT)

Copyright © 2021 The New Alexandria Foundation

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
“Software”), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
